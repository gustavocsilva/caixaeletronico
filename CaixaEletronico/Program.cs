﻿using CaixaEletronico.Display;
using CaixaEletronico.IO;

namespace CaixaEletronico
{
    public class Program
    {
        public static BaseConsole Cns { get; set; }

        public static void Main(string[] args)
        {
            if (Cns == null)
                Cns = new ConsoleWrapper();

            WindowManager.Push(new HomeScreen());
        }
    }
}
