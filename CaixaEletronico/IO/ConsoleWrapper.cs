﻿using System;

namespace CaixaEletronico.IO
{
    public class ConsoleWrapper : BaseConsole
    {
        public override string Read()
        {
            return Console.ReadLine();
        }

        protected override void WriteLine(string message)
        {
            Console.WriteLine(message);
        }
    }
}
