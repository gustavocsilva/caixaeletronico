﻿using System.Collections.Generic;

namespace CaixaEletronico.IO
{
    public abstract class BaseConsole
    {
        public BaseConsole()
        {
            this.LinesWriten = new List<string>();
        }

        public List<string> LinesWriten { get; set; }

        protected abstract void WriteLine(string message);

        public void Write(string msg)
        {
            this.LinesWriten.Add(msg);
            this.WriteLine(msg);
        }

        public abstract string Read();
    }
}