﻿using System.Collections.Generic;

namespace CaixaEletronico.Display
{
    public static class WindowManager
    {
        private static Stack<IScreen> stack = new Stack<IScreen>();

        public static void Push(IScreen screen)
        {
            stack.Push(screen);
            screen.Run();
        }

        public static void Replace(IScreen screen)
        {
            stack.Pop();
            Push(screen);
        }

        public static void Pop()
        {
            stack.Pop();

            if (stack.Count > 0)
                stack.Peek().Run();
        }
    }
}
