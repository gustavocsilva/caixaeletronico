﻿using CaixaEletronico.Business;
using CaixaEletronico.Core;

namespace CaixaEletronico.Display
{
    internal class LoginScreen : IScreen
    {
        public LoginScreen()
        {
        }

        public void Run()
        {
            Program.Cns.Write("Digite um usuário");
            string username = Program.Cns.Read();

            Program.Cns.Write("Digite uma senha");
            string password = Program.Cns.Read();

            Client client = new ClientManager().Login(username, password);
            if (client != null)
            {
                WindowManager.Replace(new ClientScreen(client));
            }
            else
            {
                Program.Cns.Write("Usuário ou senha inválido");
            }
        }
    }
}