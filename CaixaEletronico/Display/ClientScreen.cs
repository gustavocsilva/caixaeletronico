﻿using CaixaEletronico.Business;
using CaixaEletronico.Core;

namespace CaixaEletronico.Display
{
    public class ClientScreen : IScreen
    {
        public ClientScreen(Client client)
        {
            this.Client = client;
            this.Account = new AccountManager().Find(client);
        }

        public Client Client { get; set; }

        public Account Account { get; set; }

        public void Run()
        {
            Program.Cns.Write(string.Format("Bem vindo {0}", this.Client.Username));

            Program.Cns.Write("1 - Saldo");
            Program.Cns.Write("2 - Deposito");
            Program.Cns.Write("3 - Saque");
            Program.Cns.Write("4 - Transferencia");
            Program.Cns.Write("5 - Logout");
            Program.Cns.Write("Q - Sair");

            string key = Program.Cns.Read();

            var manager = new AccountManager();

            switch (key)
            {
                case "1":
                    Program.Cns.Write(string.Format("Saldo: {0}", manager.Balance(this.Account)));
                    this.Run();
                    break;
                case "2":
                    Deposit();
                    this.Run();
                    break;
                case "3":
                    Withdraw();
                    this.Run();
                    break;
                case "4":
                    Transfer();
                    this.Run();
                    break;
                case "5":
                    Logout();
                    break;
            }
        }

        private void Deposit()
        {
            var manager = new AccountManager();

            Program.Cns.Write("Digite o valor a ser depositado");
            string deposit = Program.Cns.Read();
            decimal value;
            if (decimal.TryParse(deposit, out value))
            {
                decimal balance = manager.Deposit(this.Account, value);
                Program.Cns.Write(string.Format("Deposito feito com sucesso. Saldo {0}", balance));
            }
            else
            {
                Program.Cns.Write("Valor inválido");
            }
        }

        private void Withdraw()
        {
            var manager = new AccountManager();

            Program.Cns.Write("Digite o valor a ser retirado");
            string withdraw = Program.Cns.Read();
            decimal value;
            if (decimal.TryParse(withdraw, out value))
            {
                decimal balance = manager.Withdraw(this.Account, value);
                Program.Cns.Write(string.Format("Saque feito com sucesso. Saldo {0}", balance));
            }
            else
            {
                Program.Cns.Write("Valor inválido");
            }
        }

        private void Transfer()
        {
            var manager = new AccountManager();
            Program.Cns.Write("Digite a conta destino");
            string accountText = Program.Cns.Read();


            int accountNumber;
            Account accountToTransfer = null;
            if (int.TryParse(accountText, out accountNumber))
            {
                accountToTransfer = manager.Find(accountNumber);
            }

            if (accountToTransfer == null)
            {
                Program.Cns.Write("Conta inexistente");
                return;
            }

            Program.Cns.Write("Digite o valor a ser transferido");
            string transfer = Program.Cns.Read();
            decimal value;
            if (decimal.TryParse(transfer, out value))
            {
                manager.Transfer(this.Account, accountToTransfer, value);
                Program.Cns.Write("Valor transferido com sucesso");
            }
            else
            {
                Program.Cns.Write("Valor inválido");
            }
        }

        private void Logout()
        {
            WindowManager.Pop();
        }
    }
}