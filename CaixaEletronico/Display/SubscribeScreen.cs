﻿using CaixaEletronico.Business;
namespace CaixaEletronico.Display
{
    internal class SubscribeScreen : IScreen
    {
        public SubscribeScreen()
        {
        }


        public void Run()
        {
            Program.Cns.Write("Cadastrar um novo usuário");
            Program.Cns.Write("Digite um usuário");
            string username = Program.Cns.Read();

            Program.Cns.Write("Digite uma senha");
            string password = Program.Cns.Read();

            Program.Cns.Write("Digite um documento");
            string document = Program.Cns.Read();

            Program.Cns.Write("Digite F para pessoa fisica ou J para juridica");
            string type = Program.Cns.Read();

            string message = new ClientManager().Subscribe(username, password, document, type);
            Program.Cns.Write(message);

            WindowManager.Pop();
        }
    }
}