﻿namespace CaixaEletronico.Display
{
    public class HomeScreen : IScreen
    {
        public void Run()
        {
            Program.Cns.Write("Bem vindo ao banco");
            Program.Cns.Write("1 - Login");
            Program.Cns.Write("2 - Cadastrar");
            Program.Cns.Write("Q - Sair");

            string key = Program.Cns.Read();
            if (key == "1")
                WindowManager.Push(new LoginScreen());
            else if (key == "2")
                WindowManager.Push(new SubscribeScreen());
            else if (key != "Q")
            {
                Program.Cns.Write("Código Inválido");
                this.Run();
            }
        }
    }
}