﻿using CaixaEletronico.Core;
using CaixaEletronico.Data;
using CaixaEletronico.Data.Database;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace CaixaEletronico.Teste.NUnit
{
    [TestClass]
    public class UnitTest1
    {
        [TestInitialize]
        public void Setup()
        {
            new ClientRepository().Clear();
            DatabaseFactory.IsTest = true;
        }

        [TestMethod]
        public void Home()
        {
            var console = new TestConsole(new List<string> { "Q" });
            Program.Cns = console;

            Program.Main(null);

            Assert.AreEqual(4, console.LinesWriten.Count);
            ValidateHome(console.LinesWriten);
        }

        [TestMethod]
        public void Home_CodigoInvalido()
        {
            var console = new TestConsole(new List<string> { "A", "Q" });
            Program.Cns = console;

            Program.Main(null);

            ValidateHome(console.LinesWriten);
            Assert.AreEqual("Código Inválido", console.LinesWriten[4]);
        }

        [TestMethod]
        public void Subscribe()
        {
            var console = new TestConsole(new List<string> { "2", "Gustavo", "123", "456", "F", "Q" });
            Program.Cns = console;

            Program.Main(null);

            ValidateHome(console.LinesWriten);

            Assert.AreEqual("Cadastrar um novo usuário", console.LinesWriten[4]);
            Assert.AreEqual("Digite um usuário", console.LinesWriten[5]);
            Assert.AreEqual("Digite uma senha", console.LinesWriten[6]);
            Assert.AreEqual("Digite um documento", console.LinesWriten[7]);
            Assert.AreEqual("Digite F para pessoa fisica ou J para juridica", console.LinesWriten[8]);
            Assert.AreEqual("Usuário cadastrado com sucesso", console.LinesWriten[9]);

            var repository = new ClientRepository();
            var client = repository.Get().First();
            Assert.AreEqual("Gustavo", client.Username);
            Assert.AreEqual("123", client.Password);
            Assert.AreEqual("456", client.Document);
            Assert.AreEqual(ClientType.PF, client.Type);

            var account = new AccountRepository().Get().First();
            Assert.AreEqual(account.Client, client);
        }

        [TestMethod]
        public void SubscribeInvalidType()
        {
            var console = new TestConsole(new List<string> { "2", "Gustavo", "123", "456", "X", "Q" });
            Program.Cns = console;

            Program.Main(null);

            ValidateHome(console.LinesWriten);

            Assert.AreEqual("Cadastrar um novo usuário", console.LinesWriten[4]);
            Assert.AreEqual("Digite um usuário", console.LinesWriten[5]);
            Assert.AreEqual("Digite uma senha", console.LinesWriten[6]);
            Assert.AreEqual("Digite um documento", console.LinesWriten[7]);
            Assert.AreEqual("Digite F para pessoa fisica ou J para juridica", console.LinesWriten[8]);
            Assert.AreEqual("Tipo inválido", console.LinesWriten[9]);

            var repository = new ClientRepository();
            var clients = repository.Get();
            Assert.AreEqual(0, clients.Count());
        }

        [TestMethod]
        public void SubscribeDuplicateUsername()
        {
            var repository = new ClientRepository();
            repository.Insert(new Client
            {
                Username = "Gustavo"
            });

            var console = new TestConsole(new List<string> { "2", "Gustavo", "123", "456", "F", "Q" });
            Program.Cns = console;

            Program.Main(null);

            ValidateHome(console.LinesWriten);

            Assert.AreEqual("Cadastrar um novo usuário", console.LinesWriten[4]);
            Assert.AreEqual("Digite um usuário", console.LinesWriten[5]);
            Assert.AreEqual("Digite uma senha", console.LinesWriten[6]);
            Assert.AreEqual("Digite um documento", console.LinesWriten[7]);
            Assert.AreEqual("Digite F para pessoa fisica ou J para juridica", console.LinesWriten[8]);
            Assert.AreEqual("Usuário já existente", console.LinesWriten[9]);
        }

        [TestMethod]
        public void Login()
        {
            CreateClient();

            var console = new TestConsole(new List<string> { "1", "Gustavo", "123", "Q" });
            Program.Cns = console;

            Program.Main(null);

            ValidateHome(console.LinesWriten);

            Assert.AreEqual("Digite um usuário", console.LinesWriten[4]);
            Assert.AreEqual("Digite uma senha", console.LinesWriten[5]);

            Assert.AreEqual("Bem vindo Gustavo", console.LinesWriten[6]);
        }

        [TestMethod]
        public void InvalidLogin()
        {
            var repository = new ClientRepository();
            repository.Insert(new Client
            {
                Username = "Gustavo",
                Password = "123"
            });

            var console = new TestConsole(new List<string> { "1", "Gustavo", "456", "Q" });
            Program.Cns = console;

            Program.Main(null);

            Assert.AreEqual("Bem vindo ao banco", console.LinesWriten[0]);
            Assert.AreEqual("1 - Login", console.LinesWriten[1]);
            Assert.AreEqual("2 - Cadastrar", console.LinesWriten[2]);
            Assert.AreEqual("Q - Sair", console.LinesWriten[3]);

            Assert.AreEqual("Digite um usuário", console.LinesWriten[4]);
            Assert.AreEqual("Digite uma senha", console.LinesWriten[5]);

            Assert.AreEqual("Usuário ou senha inválido", console.LinesWriten[6]);
        }

        [TestMethod]
        public void Balance()
        {
            CreateClient();

            var console = new TestConsole(new List<string> { "1", "Gustavo", "123", "1", "Q" });
            Program.Cns = console;

            Program.Main(null);

            ValidateHome(console.LinesWriten);

            Assert.AreEqual("Digite um usuário", console.LinesWriten[4]);
            Assert.AreEqual("Digite uma senha", console.LinesWriten[5]);

            Assert.AreEqual("Bem vindo Gustavo", console.LinesWriten[6]);

            ValidateClientScreen(console.LinesWriten, 7);

            Assert.AreEqual("Saldo: 8359", console.LinesWriten[13]);
        }

        [TestMethod]
        public void Deposit()
        {
            CreateClient();

            var console = new TestConsole(new List<string> { "1", "Gustavo", "123", "2", "100", "Q" });
            Program.Cns = console;

            Program.Main(null);

            ValidateHome(console.LinesWriten);

            Assert.AreEqual("Digite um usuário", console.LinesWriten[4]);
            Assert.AreEqual("Digite uma senha", console.LinesWriten[5]);

            Assert.AreEqual("Bem vindo Gustavo", console.LinesWriten[6]);

            ValidateClientScreen(console.LinesWriten, 7);

            Assert.AreEqual("Digite o valor a ser depositado", console.LinesWriten[13]);
            Assert.AreEqual("Deposito feito com sucesso. Saldo 8459", console.LinesWriten[14]);

            var account = new AccountRepository().Get().First();
            Assert.AreEqual(8459, account.Balance);
        }

        [TestMethod]
        public void InvalidDeposit()
        {
            CreateClient();

            var console = new TestConsole(new List<string> { "1", "Gustavo", "123", "2", "AAAAA", "Q" });
            Program.Cns = console;

            Program.Main(null);

            ValidateHome(console.LinesWriten);

            Assert.AreEqual("Digite um usuário", console.LinesWriten[4]);
            Assert.AreEqual("Digite uma senha", console.LinesWriten[5]);

            Assert.AreEqual("Bem vindo Gustavo", console.LinesWriten[6]);

            ValidateClientScreen(console.LinesWriten, 7);

            Assert.AreEqual("Digite o valor a ser depositado", console.LinesWriten[13]);
            Assert.AreEqual("Valor inválido", console.LinesWriten[14]);

            var account = new AccountRepository().Get().First();
            Assert.AreEqual(8359, account.Balance);
        }

        [TestMethod]
        public void Withdraw()
        {
            CreateClient();

            var console = new TestConsole(new List<string> { "1", "Gustavo", "123", "3", "100", "Q" });
            Program.Cns = console;

            Program.Main(null);

            ValidateHome(console.LinesWriten);

            Assert.AreEqual("Digite um usuário", console.LinesWriten[4]);
            Assert.AreEqual("Digite uma senha", console.LinesWriten[5]);

            Assert.AreEqual("Bem vindo Gustavo", console.LinesWriten[6]);

            ValidateClientScreen(console.LinesWriten, 7);

            Assert.AreEqual("Digite o valor a ser retirado", console.LinesWriten[13]);
            Assert.AreEqual("Saque feito com sucesso. Saldo 8259", console.LinesWriten[14]);

            var account = new AccountRepository().Get().First();
            Assert.AreEqual(8259, account.Balance);
        }

        [TestMethod]
        public void InvalidWithdraw()
        {
            CreateClient();

            var console = new TestConsole(new List<string> { "1", "Gustavo", "123", "3", "AAAAA", "Q" });
            Program.Cns = console;

            Program.Main(null);

            ValidateHome(console.LinesWriten);

            Assert.AreEqual("Digite um usuário", console.LinesWriten[4]);
            Assert.AreEqual("Digite uma senha", console.LinesWriten[5]);

            Assert.AreEqual("Bem vindo Gustavo", console.LinesWriten[6]);

            ValidateClientScreen(console.LinesWriten, 7);

            Assert.AreEqual("Digite o valor a ser retirado", console.LinesWriten[13]);
            Assert.AreEqual("Valor inválido", console.LinesWriten[14]);

            var account = new AccountRepository().Get().First();
            Assert.AreEqual(8359, account.Balance);
        }

        [TestMethod]
        public void Transfer()
        {
            CreateClient("Teste", "456");
            CreateClient();

            var console = new TestConsole(new List<string> { "1", "Gustavo", "123", "4", "1", "100", "Q" });
            Program.Cns = console;

            Program.Main(null);

            ValidateHome(console.LinesWriten);

            Assert.AreEqual("Digite um usuário", console.LinesWriten[4]);
            Assert.AreEqual("Digite uma senha", console.LinesWriten[5]);

            Assert.AreEqual("Bem vindo Gustavo", console.LinesWriten[6]);

            ValidateClientScreen(console.LinesWriten, 7);

            Assert.AreEqual("Digite a conta destino", console.LinesWriten[13]);
            Assert.AreEqual("Digite o valor a ser transferido", console.LinesWriten[14]);
            Assert.AreEqual("Valor transferido com sucesso", console.LinesWriten[15]);

            var a1 = new AccountRepository().Get().First(a => a.Id == 1);
            Assert.AreEqual(8459, a1.Balance);

            var a2 = new AccountRepository().Get().First(a => a.Id == 2);
            Assert.AreEqual(8259, a2.Balance);
        }

        [TestMethod]
        public void TransferInvalidValue()
        {
            CreateClient("Teste", "456");
            CreateClient();

            var console = new TestConsole(new List<string> { "1", "Gustavo", "123", "4", "1", "abcd", "Q" });
            Program.Cns = console;

            Program.Main(null);

            ValidateHome(console.LinesWriten);

            Assert.AreEqual("Digite um usuário", console.LinesWriten[4]);
            Assert.AreEqual("Digite uma senha", console.LinesWriten[5]);

            Assert.AreEqual("Bem vindo Gustavo", console.LinesWriten[6]);

            ValidateClientScreen(console.LinesWriten, 7);

            Assert.AreEqual("Digite a conta destino", console.LinesWriten[13]);
            Assert.AreEqual("Digite o valor a ser transferido", console.LinesWriten[14]);
            Assert.AreEqual("Valor inválido", console.LinesWriten[15]);

            var a1 = new AccountRepository().Get().First(a => a.Id == 1);
            Assert.AreEqual(8359, a1.Balance);

            var a2 = new AccountRepository().Get().First(a => a.Id == 2);
            Assert.AreEqual(8359, a2.Balance);
        }

        [TestMethod]
        public void TransferInvalidAccount()
        {
            CreateClient("Teste", "456");
            CreateClient();

            var console = new TestConsole(new List<string> { "1", "Gustavo", "123", "4", "8", "100", "Q" });
            Program.Cns = console;

            Program.Main(null);

            ValidateHome(console.LinesWriten);

            Assert.AreEqual("Digite um usuário", console.LinesWriten[4]);
            Assert.AreEqual("Digite uma senha", console.LinesWriten[5]);

            Assert.AreEqual("Bem vindo Gustavo", console.LinesWriten[6]);

            ValidateClientScreen(console.LinesWriten, 7);

            Assert.AreEqual("Digite a conta destino", console.LinesWriten[13]);
            Assert.AreEqual("Conta inexistente", console.LinesWriten[14]);

            var a1 = new AccountRepository().Get().First(a => a.Id == 1);
            Assert.AreEqual(8359, a1.Balance);

            var a2 = new AccountRepository().Get().First(a => a.Id == 2);
            Assert.AreEqual(8359, a2.Balance);
        }

        [TestMethod]
        public void TransferInvalidAccountNumber()
        {
            CreateClient("Teste", "456");
            CreateClient();

            var console = new TestConsole(new List<string> { "1", "Gustavo", "123", "4", "asfafasf", "100", "Q" });
            Program.Cns = console;

            Program.Main(null);

            ValidateHome(console.LinesWriten);

            Assert.AreEqual("Digite um usuário", console.LinesWriten[4]);
            Assert.AreEqual("Digite uma senha", console.LinesWriten[5]);

            Assert.AreEqual("Bem vindo Gustavo", console.LinesWriten[6]);

            ValidateClientScreen(console.LinesWriten, 7);

            Assert.AreEqual("Digite a conta destino", console.LinesWriten[13]);
            Assert.AreEqual("Conta inexistente", console.LinesWriten[14]);

            var a1 = new AccountRepository().Get().First(a => a.Id == 1);
            Assert.AreEqual(8359, a1.Balance);

            var a2 = new AccountRepository().Get().First(a => a.Id == 2);
            Assert.AreEqual(8359, a2.Balance);
        }

        [TestMethod]
        public void Logout()
        {
            CreateClient();

            var console = new TestConsole(new List<string> { "1", "Gustavo", "123", "5", "Q" });
            Program.Cns = console;

            Program.Main(null);

            ValidateHome(console.LinesWriten);

            Assert.AreEqual("Digite um usuário", console.LinesWriten[4]);
            Assert.AreEqual("Digite uma senha", console.LinesWriten[5]);

            Assert.AreEqual("Bem vindo Gustavo", console.LinesWriten[6]);

            ValidateClientScreen(console.LinesWriten, 7);
            ValidateHome(console.LinesWriten, 13);
        }

        private void ValidateHome(List<string> linesWriten, int index = 0)
        {
            Assert.AreEqual("Bem vindo ao banco", linesWriten[index]);
            Assert.AreEqual("1 - Login", linesWriten[index + 1]);
            Assert.AreEqual("2 - Cadastrar", linesWriten[index + 2]);
            Assert.AreEqual("Q - Sair", linesWriten[index + 3]);
        }

        private void ValidateClientScreen(List<string> linesWriten, int index)
        {
            Assert.AreEqual("1 - Saldo", linesWriten[index]);
            Assert.AreEqual("2 - Deposito", linesWriten[index + 1]);
            Assert.AreEqual("3 - Saque", linesWriten[index + 2]);
            Assert.AreEqual("4 - Transferencia", linesWriten[index + 3]);
            Assert.AreEqual("5 - Logout", linesWriten[index + 4]);
            Assert.AreEqual("Q - Sair", linesWriten[index + 5]);
        }

        private Client CreateClient()
        {
            return CreateClient("Gustavo", "123");
        }

        private Client CreateClient(string username, string password)
        {
            var repository = new ClientRepository();
            var client = new Client
            {
                Username = username,
                Password = password
            };
            repository.Insert(client);
            new AccountRepository().Insert(new Account(client)
            {
                Balance = 8359
            });

            return client;
        }
    }
}