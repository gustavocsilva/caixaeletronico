﻿namespace CaixaEletronico.Core
{
    public class Client : IEntity
    {
        public Client()
        {
        }

        public int Id { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string Document { get; set; }

        public ClientType Type { get; set; }
    }
}
