﻿namespace CaixaEletronico.Core
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}
