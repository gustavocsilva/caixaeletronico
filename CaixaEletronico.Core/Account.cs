﻿namespace CaixaEletronico.Core
{
    public class Account : IEntity
    {
        public Account()
        {

        }

        public Account(Client client)
        {
            this.Client = client;
        }

        public int Id { get; set; }

        public decimal Balance { get; set; }

        public Client Client { get; set; }
    }
}
