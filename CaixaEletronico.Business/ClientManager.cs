﻿using CaixaEletronico.Core;
using CaixaEletronico.Data.Database;
using System.Linq;

namespace CaixaEletronico.Business
{
    public class ClientManager
    {
        public Client Find(string username)
        {
            var repository = new ClientRepository();
            return repository.Get().SingleOrDefault(cliente => cliente.Username == username);
        }

        public string Subscribe(string username, string senha, string document, string type)
        {
            if (type != "F" && type != "J")
            {
                return "Tipo inválido";
            }

            var duplicate = this.Find(username);
            if (duplicate != null)
            {
                return "Usuário já existente";
            }

            var repository = new ClientRepository();
            var client = new Client
            {
                Username = username,
                Password = senha,
                Document = document,
                Type = type == "F" ? ClientType.PF : ClientType.PJ
            };

            repository.Insert(client);

            var account = new Account(client);
            new AccountRepository().Insert(account);

            return "Usuário cadastrado com sucesso";
        }

        public Client Login(string username, string password)
        {
            var repository = new ClientRepository();
            return repository.Get().SingleOrDefault(client => client.Username == username && client.Password == password);
        }
    }
}
