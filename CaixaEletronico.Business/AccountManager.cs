﻿using CaixaEletronico.Core;
using CaixaEletronico.Data.Database;
using System;
using System.Linq;

namespace CaixaEletronico.Business
{
    public class AccountManager
    {
        public Account Find(int id)
        {
            return new AccountRepository().Get().SingleOrDefault(a => a.Id.Equals(id));
        }

        public Account Find(Client client)
        {
            return new AccountRepository().Get().Single(a => a.Client.Id.Equals(client.Id));
        }

        public decimal Balance(Account account)
        {
            return account.Balance;
        }

        public decimal Deposit(Account account, decimal value)
        {
            account.Balance += value;
            new AccountRepository().Update(account);
            return this.Balance(account);
        }

        public decimal Withdraw(Account account, decimal value)
        {
            account.Balance -= value;
            new AccountRepository().Update(account);
            return this.Balance(account);
        }

        public void Transfer(Account account, Account accountToTransfer, decimal value)
        {
            this.Withdraw(account, value);
            this.Deposit(accountToTransfer, value);
        }
    }
}
