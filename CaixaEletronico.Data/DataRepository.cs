﻿using CaixaEletronico.Core;
using CaixaEletronico.Data.Database;
using System.Collections.Generic;

namespace CaixaEletronico.Data
{
    public abstract class DataRepository<T>
        where T : IEntity
    {
        private IDatabase _database;

        public DataRepository(IDatabase database)
        {
            this._database = database;
        }

        public void Delete(T item)
        {
            _database.Delete(item);
        }

        public List<T> Get()
        {
            return _database.Get<T>();
        }

        public void Insert(T item)
        {
            _database.Insert(item);
        }

        public void Update(T item)
        {
            _database.Update(item);
        }

        public void Clear()
        {
            _database.Clear();
        }
    }
}
