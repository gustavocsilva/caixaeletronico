﻿using CaixaEletronico.Core;

namespace CaixaEletronico.Data.Database
{
    public class AccountRepository : DataRepository<Account>
    {
        public AccountRepository()
            : base(DatabaseFactory.Get())
        {

        }

        //...
    }
}
