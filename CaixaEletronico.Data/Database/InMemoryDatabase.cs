﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CaixaEletronico.Core;

namespace CaixaEletronico.Data.Database
{
    public class InMemoryDatabase : IDatabase
    {
        private Dictionary<Type, IList<object>> _items = new Dictionary<Type, IList<object>>();

        private static InMemoryDatabase _instance;

        private InMemoryDatabase()
        {

        }

        public static InMemoryDatabase GetInstance()
        {
            if (_instance == null)
                _instance = new InMemoryDatabase();

            return _instance;
        }

        public void Clear()
        {
            _items.Clear();
        }

        public void Delete<T>(T item) where T : IEntity
        {
            _items[typeof(T)].Remove(item);
        }

        public List<T> Get<T>() where T : IEntity
        {
            if (!_items.ContainsKey(typeof(T)))
                _items.Add(typeof(T), new List<object>());

            return _items[typeof(T)].Cast<T>().ToList();
        }

        public void Insert<T>(T item) where T : IEntity
        {
            if (!_items.ContainsKey(typeof(T)))
                _items.Add(typeof(T), new List<object>());

            var list = this.Get<T>();
            item.Id = list.Any() ? list.Max(t => t.Id) + 1 : 1;

            _items[typeof(T)].Add(item);
        }

        public void Update<T>(T item) where T : IEntity
        {
            if (!_items.ContainsKey(typeof(T)))
                _items.Add(typeof(T), new List<object>());

            _items[typeof(T)].Remove(item);
            _items[typeof(T)].Add(item);
        }
    }
}
