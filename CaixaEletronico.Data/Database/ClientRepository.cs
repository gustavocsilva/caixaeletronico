﻿using CaixaEletronico.Core;

namespace CaixaEletronico.Data.Database
{
    public class ClientRepository : DataRepository<Client>
    {
        public ClientRepository()
            : base(DatabaseFactory.Get())
        {
            
        }

        //...
    }
}
