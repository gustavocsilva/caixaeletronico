﻿using CaixaEletronico.Core;
using LiteDB;
using System.Collections.Generic;
using System.Linq;

namespace CaixaEletronico.Data.Database
{
    public class LiteDBManager : IDatabase
    {
        public void Clear()
        {
            using (var db = new LiteDatabase(@"MyData.db"))
            {
                foreach (string name in db.GetCollectionNames())
                {
                    db.DropCollection(name);
                }
            }
        }

        public void Delete<T>(T item) where T : IEntity
        {
            using (var db = new LiteDatabase(@"MyData.db"))
            {
                var items = db.GetCollection<T>(typeof(T).Name);
                items.Delete(item.Id);
            }
        }

        public List<T> Get<T>() where T : IEntity
        {
            using (var db = new LiteDatabase(@"MyData.db"))
            {
                var items = db.GetCollection<T>(typeof(T).Name);
                return items.FindAll().ToList();
            }
        }

        public void Insert<T>(T item) where T : IEntity
        {
            using (var db = new LiteDatabase(@"MyData.db"))
            {
                var items = db.GetCollection<T>(typeof(T).Name);
                items.Insert(item);
            }
        }

        public void Update<T>(T item) where T : IEntity
        {
            using (var db = new LiteDatabase(@"MyData.db"))
            {
                var items = db.GetCollection<T>(typeof(T).Name);
                items.Update(item);
            }
        }
    }
}
