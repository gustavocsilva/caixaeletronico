﻿using CaixaEletronico.Data.Database;

namespace CaixaEletronico.Data
{
    public static class DatabaseFactory
    {
        public static bool IsTest { get; set; }

        public static IDatabase Get()
        {
            if (IsTest)
                return InMemoryDatabase.GetInstance();
            else
                return new LiteDBManager();
        }
    }
}
