﻿using CaixaEletronico.IO;
using System.Collections.Generic;

namespace CaixaEletronico.Test
{
    public class TestConsole : BaseConsole
    {
        public TestConsole(List<string> lines)
        {
            Lines = lines;
        }

        public List<string> Lines { get; set; }

        public int Index { get; set; }

        public override string Read()
        {
            if (Index >= this.Lines.Count)
                return null;

            string line = this.Lines[Index];
            Index++;
            return line;
        }

        protected override void WriteLine(string message)
        {
        }
    }
}
