﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;


namespace CaixaEletronico.Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Home()
        {
            var console = new TestConsole(new List<string> { "Q" });
            Program.Cns = console;

            Program.Main(null);

            Assert.AreEqual(4, console.LinesWriten.Count);
            Assert.AreEqual("Bem vindo ao banco", console.LinesWriten[0]);
            Assert.AreEqual("1 - Login", console.LinesWriten[1]);
            Assert.AreEqual("2 - Cadastrar", console.LinesWriten[2]);
            Assert.AreEqual("Q - Sair", console.LinesWriten[3]);
        }

        [TestMethod]
        public void Home_CodigoInvalido()
        {
            var console = new TestConsole(new List<string> { "A", "Q" });
            Program.Cns = console;

            Program.Main(null);

            Assert.AreEqual("Bem vindo ao banco", console.LinesWriten[0]);
            Assert.AreEqual("1 - Login", console.LinesWriten[1]);
            Assert.AreEqual("2 - Cadastrar", console.LinesWriten[2]);
            Assert.AreEqual("Q - Sair", console.LinesWriten[3]);
            Assert.AreEqual("Código Inválido", console.LinesWriten[4]);
        }

        [TestMethod]
        public void Login()
        {
            var console = new TestConsole(new List<string> { "1", "Q" });
            Program.Cns = console;

            Program.Main(null);

            Assert.AreEqual(5, console.LinesWriten.Count);
            Assert.AreEqual("Bem vindo ao banco", console.LinesWriten[0]);
            Assert.AreEqual("1 - Login", console.LinesWriten[1]);
            Assert.AreEqual("2 - Cadastrar", console.LinesWriten[2]);
            Assert.AreEqual("Q - Sair", console.LinesWriten[3]);

            Assert.AreEqual("Login", console.LinesWriten[4]);
        }
    }
}